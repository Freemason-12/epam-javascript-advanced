import { create } from './join-us-section.js';
import { validate } from './email-validator.js';
import { subscribeAjax, unsubscribeAjax } from './ajax.js';
import { CommunitySection } from './community-section.js';
import '../styles/style.css';
import your_image_join from '../assets/images/your-image-join.jpg';
import { WebsiteSection } from './web-components.js';
customElements.define('website-section', WebsiteSection);

const app_section_culture = document.querySelector('.app-section--image-culture');
const standard_section = create('standard');
const class_name = standard_section.section.className;
app_section_culture.insertAdjacentElement('afterend', standard_section.section);

const app_section_join = document.querySelector(`.${class_name}`);
const app_section_join_title = document.querySelector(`.${class_name} .app-title`);
const app_section_join_subtitle = document.querySelector(`.${class_name} .app-subtitle`);
const app_section_join_form = document.querySelector('.app-section__form-join');
const app_section_join_email_field = app_section_join_form.querySelector('[placeholder="Email"]');
const app_section_join_form_submit = app_section_join_form.querySelector('input[type="submit"]');

const app_section_about = document.querySelectorAll('.app-section')[1];
const app_section_community = await CommunitySection();
app_section_about.insertAdjacentElement('afterend', app_section_community);

function isSubscribed() {
  return localStorage.getItem('submitted') !== null;
}

function checkSubscription() {
  if (isSubscribed()) subscribeEffect();
  else unsubscribeEffect();
}

function subscribeEffect() {
  app_section_join_email_field.style.display = 'none';
  app_section_join_form_submit.value = 'unsubscribe';
}

function unsubscribeEffect() {
  app_section_join_email_field.style.display = 'inline';
  app_section_join_form_submit.value = 'subscribe';
}

async function subscribe() {
  const email = app_section_join_email_field.value;
  const response = await subscribeAjax(email);
  console.log(response);
  if (response.success) {
    subscribeEffect();
    localStorage.setItem('email', email);
    localStorage.setItem('submitted', 'true');
  } else alert(response.error);
}

async function unsubscribe() {
  const response = await unsubscribeAjax();
  console.log(response);
  if (response.success) {
    unsubscribeEffect();
    localStorage.removeItem('email');
    localStorage.removeItem('submitted');
  } else alert(response.error);
}

window.addEventListener('load', checkSubscription);

app_section_join_email_field.addEventListener('input', () => {
  const val = app_section_join_email_field.value;
  localStorage.setItem('email', val);
  if (validate(val)) {
    app_section_join_email_field.style.background = 'rgba(255, 255, 255, 0.3)';
    app_section_join_form_submit.disabled = false;
    app_section_join_form_submit.style.opacity = 1;
  } else {
    app_section_join_email_field.style.background = 'rgba(255, 120, 120, 0.3)';
    app_section_join_form_submit.disabled = true;
    app_section_join_form_submit.style.opacity = 0.5;
  }
  console.log(app_section_join_form_submit.disabled);
});

app_section_join_form.addEventListener('submit', (e) => {
  e.preventDefault();
  const val = app_section_join_email_field.value;
  if (!isSubscribed() && validate(val)) subscribe();
  else if (isSubscribed()) unsubscribe();
});

const clickEventLogger = new Worker(new URL('./worker.js', import.meta.url));

function logEvent(e) {
  console.log(e.target);
  const tagname = e.target.tagName;
  let value;
  if (tagname === 'INPUT') value = e.target.type === 'submit' ? e.target.value : e.target.placeholder;
  else value = e.target.innerText;
  clickEventLogger.postMessage({ target: tagname, content: value });
}

window.onerror = (message, url, line, column, error) => { console.log(message, url, line, column, error); };
const clickables = document.querySelectorAll('button, input');
clickables.forEach(b => { b.addEventListener('click', (e) => logEvent(e)); });

app_section_join.style = `
  background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.3)),
              url('${your_image_join}') center/cover;
  height: 440px;
  padding: 1rem;
`;
app_section_join_title.style.color = 'white';
app_section_join_subtitle.style.color = 'white';
app_section_join_form.style = `
  width: 100%;
  display: flex;
  justify-content: center;
  gap: 30px;
`;
app_section_join_email_field.style = `
  border: 0;
  background: rgba(255,255,255,0.3);
  color: white;
  padding: 0.5rem;
  width: 50%;
  display: inline-block;
`;

app_section_join_form.querySelector('input[type="submit"]').style = `
  font-family: Oswald;
  text-transform: uppercase;
  padding: 0.5rem 1rem;
  border-radius: 2rem;
`;

const media = window.matchMedia('(max-width: 768px)');
const list = app_section_community.querySelector('.list');
media.addEventListener('change', () => {
  const apply = window.innerWidth <= 768;
  app_section_join_form.style.flexDirection = apply ? 'column' : 'row';
  app_section_join_form.style.alignItems = apply ? 'center' : 'unset';
  list.style.justifyContent = apply ? 'flex-start' : 'center';
  list.querySelectorAll('.list-item').forEach((i) => {
    console.log(i);
    i.style.margin = (apply) ? '0px 30%' : '0px 2rem';
  });
});
