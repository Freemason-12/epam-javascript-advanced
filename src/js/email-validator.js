const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

/* eslint eqeqeq: "error" */
function validate(email) {
  let valid = false;
  for (const i of VALID_EMAIL_ENDINGS) {
    const regex = RegExp(`.+${i}$`);
    if (regex.test(email)) { valid = true; break; }
  }
  return valid;
}

export { VALID_EMAIL_ENDINGS, validate };
