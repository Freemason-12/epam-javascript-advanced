import { getCommunity } from './ajax.js';

async function CommunitySection() {
  const section = document.createElement('section');
  section.className = 'app-section';
  section.innerHTML = `
    <h2 class="app-title">Big Community of<br>People Like You</h2>
    <p class="app-subtitle">We're proud of our products and we're really excited <br> when we get feedback from our users</p>  
    <div class="list"></div>
`;
  const list = section.querySelector('.list');
  list.style = `
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 2rem;
    margin-bottom: 100px;
    margin-top: 57px;
    
    overflow: scroll;
    scroll-snap-type: x mandatory;
    width: 100%;
  `;
  const api_list = await getCommunity();
  for (const i of api_list) {
    const element = document.createElement('div');
    element.className = 'list-item';
    const formatted_position = i.position.split(' ');
    formatted_position.splice(2, 0, '<br>');
    element.innerHTML = `
      <img class="avatar" src="${i.avatar}"/>
      <p class="desc">Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.</p>
      <p class="fullname"><b>${i.firstName} ${i.lastName}</b></p>
      <p class="position">${formatted_position.join(' ')}</p>
    `;
    element.style = `
      text-align: center;
      width: 15rem;
      min-width: 15rem;
      scroll-snap-align: center;
    `;
    element.querySelector('.desc').style = `
      line-height: 20px;
      color: #666666;
      margin-bottom: 39px;
    `;
    element.querySelector('.fullname').style = `
      text-transform: uppercase;
      letter-spacing: 0.5;
      color: #464547;
      margin-bottom: 11px;
    `;
    element.querySelector('.position').style = `
      margin-top: 11px;
      font-size: 12px;
      line-height: 16px;
      color: #666666;
    `;
    list.append(element);
  }
  return section;
}

export { CommunitySection };
