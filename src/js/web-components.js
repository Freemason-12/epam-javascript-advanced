class WebsiteSection extends HTMLElement {
  // connectedCallback() {
  //   this.innerHTML = `
  //     <h2>${this.getAttribute('title')}</h2>
  //     <p>${this.getAttribute('description')}</p>
  //     <p>${this.getAttribute('inner-content')}</p>
  //     `;
  // }

  constructor() {
    super();
    const template = document.getElementById('section-template');
    const shadow_root = this.attachShadow({ mode: 'open' });
    shadow_root.appendChild(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return ['title', 'description', 'inner content'];
  }
}

export { WebsiteSection };
