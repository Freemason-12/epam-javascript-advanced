const address = 'http://localhost:8080/analytics';
const chunk = [];
onmessage = async (m) => {
  console.log(JSON.stringify(m.data));
  chunk.push(m.data);
  if (chunk.length >= 5) {
    const response = await fetch(`${address}/user`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(chunk)
    }).then(r => r.json());
    console.log(response);
    console.log(JSON.stringify(chunk));
    while (chunk.length > 0) chunk.pop();
  }
};
