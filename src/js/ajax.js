const address = 'http://localhost:8080';
async function subscribeAjax(email) {
  let response;
  try {
    response = await fetch(`${address}/subscribe`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email })
      });
  } catch (e) { return { error: e }; }
  return response.json();
}

async function unsubscribeAjax() {
  let response;
  try {
    response = await fetch(`${address}/unsubscribe`, { method: 'POST' });
  } catch (e) { return { error: e }; }
  return response.json();
}

async function getCommunity() {
  let response;
  try {
    response = await fetch(`${address}/community`);
  } catch (e) { return { error: e }; }
  return response.json();
}

export { subscribeAjax, unsubscribeAjax, getCommunity };
