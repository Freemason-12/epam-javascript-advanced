function SectionCreator() {
  function mockup(type, title_text, submit_button_text) {
    const mockup_element = document.createElement('section');
    mockup_element.className = `app-section--${type}`;
    mockup_element.innerHTML = `
      <h2 class="app-title">${title_text}</h2>
      <h3 class="app-subtitle">
          Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.
      </h3>
      <form class="app-section__form app-section__form-join">
          <input name="email" type="text" value="" placeholder="Email"/>
          <input name="email" type="submit" class="app-section__button" value="${submit_button_text}"/>
      </form>
    `;
    return mockup_element;
  }
  function del(section) {
    const to_remove = document.querySelector(section.className);
    if (to_remove) to_remove.parentNode.removeChild(to_remove);
  }
  return { mockup: mockup, del: del }; // eslint-disable-line object-shorthand
}

function StandardSection() {
  const creator = SectionCreator();
  const section = creator.mockup('standard', 'Join Our Program', 'subscribe');
  const remove = () => creator.del(section);
  return { section: section, remove: remove }; // eslint-disable-line object-shorthand
}

function AdvancedSection() {
  const creator = SectionCreator();
  const section = creator.mockup('advanced', 'Join Our Advanced Program', 'Subscribe to Advanced Program');
  const remove = () => creator.del(section);
  return { section: section, remove: remove }; // eslint-disable-line object-shorthand
}

function create(type) {
  switch (type) {
    case 'standard': return StandardSection();
    case 'advanced': return AdvancedSection();
    default: throw Error(`no such type: ${type}`);
  }
}

export { SectionCreator, StandardSection, AdvancedSection, create };
